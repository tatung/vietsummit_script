#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import re
import MySQLdb
import requests
import json
import traceback
from time import localtime, strftime, gmtime

isEnableVI = True
isEnableEN = True

json_gen_script = 'https://script.google.com/macros/s/AKfycbzA2IlFPJ0LNnG2vcVnwYGvnIIP590Y_CzQ9vVPK4cqdZoU5ebx/exec'
json_keys = {'ss_description_vi': "SessionContent", 'ss_name_vi':'SessionNameVI', 'ss_name_en': 'SessionNameEN', 'sp_list': 'SpeakersList', 'sp_name_vi': 'SpeakerNameVI', 'sp_name_en': 'SpeakerNameEN', 'sp_affi_vi': 'SpeakerAffiVI', 'sp_affi_en': 'SpeakerAffiEN', 'site_vi':'SiteVI', 'site_en':'SiteEN', 'title_vi':'TitleVI', 'title_vn':'TitleEN', 'abst_vi':'AbstractVI', 'abst_en':'AbstractEN', 'cmt':'Comment', 'mkt_url':'MKT_URL', 'sp_exp_vi':'SpeakerExperienceVI', 'sp_awd_vi':'SpeakerAwardsVI', 'sp_field_vi':'SpeakerFieldVI', 'sp_quote':'SpeakerQuotation'}
delimiterChar = ""

speaker_post_id_en = 3176
speaker_post_id_vi = 2996

popup_heading_start = '<h4 style="text-align: justify;">'
popup_heading_end = '</h4>'
popup_paragraph_start = '<p style="text-align: justify;">'
popup_paragraph_end = '</p>'

 # Connect to MySQL
con = MySQLdb.connect(host='localhost', db='dbzhf52k8xhqwv',
                        user='uupxah8r689qa', passwd='vnsummit2019', charset='utf8')
cur = con.cursor()


def getPhotoID(speakerName):
    photoId = 3470
    speakerName = speakerName.replace(' ', '-')

    # get id of the photo of the speaker
    sql_get_id = "SELECT ID FROM wp_posts WHERE guid LIKE \'%" + speakerName + "%\'"
    cur.execute(sql_get_id)
    records = cur.fetchall()
    if len(records) > 0:
        photoId = records[0][0]
 
    return photoId

def toHtmlSpeaker(lang, speaker, photoId):
    strHtml = "[vc_column_inner width=\"1/3\"]" + delimiterChar
    strHtml += "[tz_our_speakers use_effects=\"1\" effects_light_box=\"1\" text_thank=\"\" type_hover=\"1\" speakers_image=\"" + str(photoId) + "\" name=\"" + speaker[json_keys['sp_name_' + lang]] + "\" employment=\""+ speaker[json_keys['sp_affi_' + lang]] +"\" speakers_image_square=\"" + str(photoId) + "\"]" + delimiterChar
    #strHtml += "<span style=\"font-family: arial, helvetica, sans-serif;\">To be updated...</span>" + delimiterChar

    if speaker[json_keys['sp_exp_vi']]:
        strHtml += popup_heading_start + unicode('Quá trình công tác, kinh nghiệm làm việc', 'utf8') + popup_heading_end + delimiterChar
        strHtml += popup_paragraph_start + speaker[json_keys['sp_exp_vi']] + popup_paragraph_end + delimiterChar

    if speaker[json_keys['sp_awd_vi']]: 
        strHtml += popup_heading_start + unicode('Thành tích nổi bật trong lĩnh vực', 'utf8') + popup_heading_end + delimiterChar
        strHtml += popup_paragraph_start + speaker[json_keys['sp_awd_vi']] + popup_paragraph_end + delimiterChar

    if speaker[json_keys['sp_field_vi']]:
        strHtml += popup_heading_start + unicode('Lĩnh vực quan tâm', 'utf8') + popup_heading_end + delimiterChar
        strHtml += popup_paragraph_start + speaker[json_keys['sp_field_vi']] + popup_paragraph_end + delimiterChar
    
    if speaker[json_keys['sp_quote']]:
        strHtml += popup_heading_start + unicode('Thông điệp', 'utf8') + popup_heading_end + delimiterChar
        strHtml += popup_paragraph_start + speaker[json_keys['sp_quote']] + popup_paragraph_end + delimiterChar

    strHtml += "[/tz_our_speakers][/vc_column_inner]" + delimiterChar + delimiterChar
    return strHtml

def toHmtlSession(lang, session):
    strHtml = "[vc_row el_class=\"guests_row\"]" + delimiterChar
    strHtml += "[vc_column]" + delimiterChar
    strHtml += "[vc_row_inner]" + delimiterChar
    strHtml += "[tz_title_meetup section_type=\"type4\" title=\"" + session[json_keys['ss_name_' + lang]] + "\"][/tz_title_meetup]" + delimiterChar
    if lang == "vi":
        strHtml += "[vc_column_inner][vc_column_text]" + "<p style=\"text-align: justify;\">" + session[json_keys['ss_description_' + lang]] + "</p>"  + "[/vc_column_text][/vc_column_inner]\\n\\n\&nbsp;\\n\\n" + delimiterChar
    for sp in session[json_keys['sp_list']]:
        strHtml += toHtmlSpeaker(lang, sp, getPhotoID(sp[json_keys['sp_name_en']]))

    strHtml += "[/vc_row_inner][/vc_column][/vc_row]" + delimiterChar + delimiterChar
    return strHtml

def toHtml(lang, sessionList):
    strHtml = "<p>[vc_row content_placement=\"middle\" css=\".vc_custom_1568381598672{padding-top: 0px !important;padding-right: 0px<br /> !important;padding-bottom: 0px !important;padding-left: 0px !important;}\" el_class=\"home9\" el_id=\"row_banner\"][vc_column css=\".vc_custom_1567735448956{padding-top: 0px !important;padding-right: 0px !important;padding-left: 0px<br />!important;background-image: url(https://vietnamsummit.org/wp-content/uploads/2019/09/banner-5.png?id=2684)<br />!important;}\"][vc_row_inner][vc_column_inner][tz-header home_link=\"1\" type=\"1\" type_position=\"relative\" show_hide_cart=\"2\" select_menu=\"Primary Menu\" click_menu_one_page=\"1\" show_hide_contact_info=\"4\" show_hide_search=\"2\" logo_image=\"3152\"][/vc_column_inner][/vc_row_inner][vc_single_image image=\"2511\" img_size=\"large\" alignment=\"center\" css_animation=\"fadeInDown\" css=\".vc_custom_1567748979441{padding-top: 5% !important;}\" el_id=\"header_image\"][vc_row_inner][vc_column_inner][tel_countdown tel_countdown_year=\"2019\" tel_countdown_month=\"Nov\" tel_countdown_day=\"16\" tel_countdown_hour=\"9\" tel_countdown_minute=\"0\" tel_countdown_second=\"0\" css_animation=\"fadeInUp\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row css=\".vc_custom_1568785343935{padding-top: 0px !important;padding-bottom: 0px<br />!important;}\"][vc_column][vc_empty_space][/vc_column][/vc_row][vc_row][vc_column][vc_column_text]</p><p>[/vc_column_text][/vc_column][/vc_row]"

    for ss in sessionList:
        strHtml += toHmtlSession(lang, ss)

    # strHtml += "[vc_column_inner width=\"1/6\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row tz_row_type=\"boxed\" full_width=\"stretch_row_content_no_spaces\" equal_height=\"yes\" content_placement=\"middle\" css_animation=\"fadeInUp\" css=\".vc_custom_1568599487223{padding-top: 15px !important;padding-bottom: 15px !important;background-color: #242732 !important;}\"][vc_column][tz_title_meetup title=\"Mọi chi tiết xin liên hệ:\" color_title=\"#ffffff\"][/tz_title_meetup][vc_row_inner][vc_column_inner][evc_icon_with_text type=\"icon-left\" title_tag=\"h4\" custom_icon=\"3279\" custom_link=\"url:https%3A%2F%2Fwww.fb.me%2Fvietnam.summit.in.japan%2F|||\" custom_class=\"footer-icon\" title=\"Fanpage: fb.me/vietnam.summit.in.japan/\" title_color=\"#ffffff\"][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner][evc_icon_with_text type=\"icon-left\" title_tag=\"h4\" custom_icon=\"3278\" custom_link=\"url:mailto%3A%20organizers%40vietnamsummit.org|||\" custom_class=\"footer-icon\" title=\"Email: organizers@vietnamsummit.org\" title_color=\"#ffffff\"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]"
    return strHtml

try:  

    # Get JSON data of sessions from google script:
    r = requests.get(json_gen_script)
    j = json.loads(r.text)

    speakerPage = toHtml('en', j).replace("\"", "\\\"")
    if isEnableEN:
        # update speaker page english
        sql = 'UPDATE wp_posts SET post_content = "' + speakerPage + '" WHERE ID =' + str(speaker_post_id_en) + ';'
        cur.execute(sql)
        con.commit()
        print 'done English'
    else:
        print 'English script disable'

    speakerPage = toHtml('vi', j).replace("\"", "\\\"")
    if isEnableVI:
        # update speaker page vietnamese
        sql = 'UPDATE wp_posts SET post_content = "' + speakerPage + '" WHERE ID =' + str(speaker_post_id_vi) + ';'
        cur.execute(sql)
        con.commit()
        print 'done Vietnamese'
    else:
        print 'Vietnamese script disable'

   
    cur.close()
    con.close()

# except ValueError:
#  send_email.send_email(user,password,addr,"ValueError:may be YYYY/MM")
except Exception as e:
    #send_email.send_email(user,password,addr,"Something is Wrong")
    print ('something is wrong')
    # print(e)
    traceback.print_exc()

cur.close()



